# -*- coding: utf-8 -*-
from django.contrib import admin

from business.models import StatusBusiness,Business, Vacancy, VacancyCompetence,OrderSpecialist,StatusOrderSpecialist

class StatusBisinessAdmin(admin.ModelAdmin):
    list_filter = ('price',)
    list_display = ('name', 'description', 'price')
    search_fields = ('price', 'name')
    # date_hierarchy = 'date_close'


admin.site.register(StatusBusiness, StatusBisinessAdmin)

class BusinessAdmin(admin.ModelAdmin):
    list_filter = ('city',)
    list_display = ('name', 'user', 'site', 'city', 'address', 'status')
    search_fields = ('name', 'city',)
    # date_hierarchy = 'date_close'

admin.site.register(Business,BusinessAdmin)

# нужна ли сортировка кроме даты
class VacancyAdmin(admin.ModelAdmin):
    list_filter = ('business',)
    list_display = ('name', 'business', 'link', 'date_start', 'date_end')
    # search_fields = ('business','name')
    date_hierarchy = 'date_end'


admin.site.register(Vacancy, VacancyAdmin)


class VacancyCompetenceAdmin(admin.ModelAdmin):
    list_filter = ('competence',)
    list_display = ('vacancy', 'competence', 'value_expected')
    search_fields = ('vacancy', 'competence')
    # date_hierarchy = 'date_end'


admin.site.register(VacancyCompetence, VacancyCompetenceAdmin)


class OrderSpecialistAdmin(admin.ModelAdmin):
    list_filter = ('status__name', 'education', 'group_education')
    list_display = ('vacancy', 'education', 'group_education', 'specialist', 'status')
    search_fields = ('vacancy', 'education')
    # date_hierarchy = 'date_end'


admin.site.register(OrderSpecialist, OrderSpecialistAdmin)


class StatusOrderSpecialistAdmin(admin.ModelAdmin):
    # list_filter = ('status__name','education__name','group_education__name')
    list_display = ('name', 'description')
    # search_fields = ('vacancy__name','education__name')
    # date_hierarchy = 'date_end'

admin.site.register(StatusOrderSpecialist, StatusOrderSpecialistAdmin)
