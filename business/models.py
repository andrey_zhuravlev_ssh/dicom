# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from education.models import GroupEducation, Education
from specialist.models import Specialist
from competence.models import Competence, City


# Reference table for status of business (gold, silver, simple, block etc.)
class StatusBusiness(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")
    price = models.FloatField(verbose_name="Цена")

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Статус работодателя (в системе)"


class Business(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название организации")
    parent_business = models.ForeignKey('self', verbose_name="Название родительской организации", null=True, blank=True,
                                        related_name='base_business', on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(verbose_name="Описание")
    image = models.ImageField(upload_to='uploads/images', verbose_name="Картинка", blank=True, null=True)
    site = models.URLField(verbose_name="Сайт", blank=True, null=True)
    city = models.ForeignKey(City, verbose_name="Населенный пункт")
    address = models.CharField(max_length=2048, verbose_name="Адрес", blank=True, null=True)
    status = models.ForeignKey(StatusBusiness, verbose_name="Статус (подписка)", blank=True, null=True)
    geo = models.CharField(max_length=1024, verbose_name="Координаты",blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Работодатель"


class Vacancy(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    business = models.ForeignKey(Business, verbose_name="Работодатель")
    cost = models.FloatField(verbose_name="Зарплата", blank=True, null=True)
    link = models.URLField(verbose_name="Ссылка на вакансию", blank=True, null=True)
    description = models.TextField(verbose_name="Описание")
    date_start = models.DateField(verbose_name="Дата создания", auto_now_add=True)
    date_end = models.DateField(verbose_name="Дата закрытия", blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Вакансия"

    def get_competence(self):
        return VacancyCompetence.objects.filter(vacancy=self)


# Relation table vacancy-competence
class VacancyCompetence(models.Model):
    vacancy = models.ForeignKey(Vacancy, verbose_name="Вакансия")
    competence = models.ForeignKey(Competence, verbose_name="Компетенция")
    value_expected = models.TextField(verbose_name="Ожидаемый результат", blank=True, null=True)
    comment = models.TextField(verbose_name="Комментарии", blank=True, null=True)


   # def __unicode__(self):
    #    return str(self.vacancy) + u" " + str(self.competence)

    class Meta:
        ordering = ["vacancy__name"]
        verbose_name_plural = u"Компетенции для вакансии"


# Reference table for status order of specialist  (gold, silver, simple, block etc.)
class StatusOrderSpecialist(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Статус заявки на специалиста"


class OrderSpecialist(models.Model):
    vacancy = models.ForeignKey(Vacancy, verbose_name="Вакансия")
    education = models.ForeignKey(Education, verbose_name="Образовательное учреждение")
    group_education = models.ForeignKey(GroupEducation, verbose_name="Направление", blank=True, null=True)
    specialist = models.ForeignKey(Specialist, verbose_name="Специалист", blank=True, null=True)
    status = models.ForeignKey(StatusOrderSpecialist, verbose_name="Статус")
    comment = models.TextField(verbose_name="Комментарии", blank=True, null=True)

#    def __unicode__(self):
#        return self.vacancy + " " + self.education

    class Meta:
        ordering = ["vacancy__name"]
        verbose_name_plural = u"Заявка на специалиста"