# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render
from django.shortcuts import redirect
from django.template import RequestContext
from django.http import Http404

from dicom.settings import TEST_LOGIN
from business.models import OrderSpecialist, Vacancy


def login_business(request):
    try:
        if not TEST_LOGIN:
            return request.user.business
    except:
        raise Http404()

@login_required
def lk_business(request, template_name="business/lk_business.html"):
    current_user = login_business(request)
    company_vacancies = Vacancy.objects.filter(business__id=current_user.id)
    company_order = []
    for item in company_vacancies:
        tmp_company_order = OrderSpecialist.objects.filter(vacancy=item)
        if tmp_company_order:
            for tmp_item in tmp_company_order:
                company_order.append(tmp_item)
    navigation_template = "tags/navigation_business.html"

    return render(request, template_name, locals())


@login_required
def business_order(request, template_name="business/business_order.html"):
    current_user = login_business(request)

    navigation_template = "tags/navigation_business.html"

    company_vacancies = Vacancy.objects.filter(business__id=current_user.id)
    company_order = []
    for item in company_vacancies:
        tmp_company_order = OrderSpecialist.objects.filter(vacancy=item)
        if tmp_company_order:
            for tmp_item in tmp_company_order:
                company_order.append(tmp_item)

    return render(request, template_name, locals())


@login_required
def business_vacancy(request, template_name="business/business_vacancy.html"):
    current_user = login_business(request)

    company_vacancies = Vacancy.objects.filter(business__id=current_user.id)
    company_order = []
    for item in company_vacancies:
        tmp_company_order = OrderSpecialist.objects.filter(vacancy=item)
        if tmp_company_order:
            for tmp_item in tmp_company_order:
                company_order.append(tmp_item)

    navigation_template = "tags/navigation_business.html"

    return render(request, template_name, locals())