# -*- coding: utf-8 -*-
from django.db import models

from competence.models import Competence
from business.models import Business
from education.models import Education
from specialist.models import Specialist


class TestCompetence(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")
    competence = models.ForeignKey(Competence, related_name="competence", verbose_name="Компетенция")
    business = models.ForeignKey(Business, related_name="business", verbose_name="Работодатель")
    education = models.ForeignKey(Education, related_name="education", verbose_name="Образовательное учреждение")

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Тест"


# Relation table specialist-competence-test (+ history for specialist)
class SpecialistCompetence(models.Model):
    specialist = models.ForeignKey(Specialist, verbose_name="Специалист")
    competence = models.ForeignKey(Competence, verbose_name="Компетенция")
    test = models.ForeignKey(TestCompetence,  verbose_name="Тест", blank=True, null=True)
    value = models.IntegerField(verbose_name="Значение")
    date = models.DateField(verbose_name="Дата", auto_now_add=True)
    date_end = models.DateField(verbose_name="Дата закрытия", blank=True, null=True) # I will no longer work on this competence
    comment = models.TextField(verbose_name="Комментарий", blank=True, null=True)


    def __unicode__(self):
        return self.specialist.user.get_full_name() + " " + self.competence.name

    class Meta:
        # ordering = ["specialist"]
        verbose_name_plural = u"Компетенции специалиста"