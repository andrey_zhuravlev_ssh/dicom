from django.contrib import admin
from test_competence.models import TestCompetence,SpecialistCompetence



class TestCompetenceAdmin(admin.ModelAdmin):
    list_filter = ('business', 'education',)
    list_display = ('name','description','competence','business', 'education',)
    search_fields = ('name',)
    # date_hierarchy = 'date_close'
admin.site.register(TestCompetence,TestCompetenceAdmin)

class SpecialistCompetenceAdmin(admin.ModelAdmin):
    list_filter = ('competence',)
    list_display = ('specialist','competence', 'test','value','date','date_end')
    search_fields = ('specialist','competence','date')
    date_hierarchy = 'date_end'
admin.site.register(SpecialistCompetence, SpecialistCompetenceAdmin)
