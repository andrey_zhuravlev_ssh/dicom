# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render
from django.shortcuts import redirect
from django.template import RequestContext
from django.http import Http404

from dicom.settings import TEST_LOGIN


@login_required
def lk_education(request, template_name="education/lk_education.html"):
    try:
        if not TEST_LOGIN:
            request.user.education
        navigation_template = "tags/navigation_education.html"
    except:
        raise Http404()
    return render(request, template_name, locals())