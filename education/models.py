# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from specialist.models import Specialist
from competence.models import Competence, City


# Reference table for status of university (gold, silver, simple, block etc.)
class StatusEducation(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")
    price = models.FloatField(verbose_name="Цена", blank=True, null=True)
    count_student = models.IntegerField(verbose_name="Минимальное количество студентов (для бесплатной подписки)", blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Статус образовательного учреждения (в системе)"


class Education(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название образовательного учреждения")
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(verbose_name="Описание")
    image = models.ImageField(upload_to='uploads/images', verbose_name="Картинка", blank=True, null=True)
    site = models.URLField(verbose_name="Сайт", blank=True, null=True)
    city = models.ForeignKey(City, verbose_name="Населенный пункт", blank=True, null=True)
    address = models.CharField(max_length=2048, verbose_name="Адрес", blank=True, null=True)
    status = models.ForeignKey(StatusEducation, verbose_name="Статус (подписка)", blank=True, null=True)
    geo = models.CharField(max_length=1024, verbose_name="Координаты", blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Образовательное учреждение"


# Reference table for standard of profession
class StandardProfession(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название стандарта")
    standard = models.URLField(verbose_name="ссылка на стандарт")
    description = models.TextField(verbose_name="Описание", blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Образовательные стандарты"


class GroupEducation(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    education = models.ForeignKey(Education,verbose_name="Образовательное учреждение")
    standard_profession = models.ForeignKey(StandardProfession,verbose_name="Образовательный стандарт")
    description = models.TextField(verbose_name="Описание", blank=True, null=True)
    date_start = models.DateField(verbose_name="Дата начала обучения", blank=True, null=True)
    date_end = models.DateField(verbose_name="Дата конца обучения", blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Группа образовательного учреждения"


# Relation table group-specialist (+ history)
class GroupEducationSpecialist(models.Model):
    group_education = models.ForeignKey(GroupEducation, verbose_name="Группа образовательного учреждения")
    specialist =  models.ForeignKey(Specialist, verbose_name="Специалист")
    description = models.TextField(verbose_name="Описание", blank=True, null=True)
    date_start = models.DateField(verbose_name="Дата зачисления")
    date_end = models.DateField(verbose_name="Дата окончания/отчисления", blank=True, null=True)

    def __unicode__(self):
        return self.group_education

    class Meta:
        ordering = ["group_education__name"]
        verbose_name_plural = u"Группа образовательного учреждения (Направление)"


# Relation table group-competence
class GroupEducationCompetence(models.Model):
    group_education = models.ForeignKey(GroupEducation, verbose_name="Группа образовательного учреждения")
    competence =  models.ForeignKey(Competence, verbose_name="Компетенция")
    comment = models.TextField(verbose_name="Комментарий", blank=True, null=True)

    def __unicode__(self):
        return self.group_education + " " + self.competence

    class Meta:
        ordering = ["group_education__name"]
        verbose_name_plural = u"Компетенции группы образовательного учреждения"