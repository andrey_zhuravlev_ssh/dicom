from django.contrib import admin

from education.models import Education , StatusEducation , StandardProfession, GroupEducation,GroupEducationSpecialist,GroupEducationCompetence


class EducationAdmin(admin.ModelAdmin):
    list_filter = ('city',)
    list_display = ('name','user', 'site', 'city', 'address','status',)
    search_fields = ('name', 'city',)
    # date_hierarchy = 'date_close'

admin.site.register(Education,EducationAdmin)


class StatusEducationAdmin(admin.ModelAdmin):
    list_filter = ('price',)
    list_display = ('name','description', 'price','count_student')
    search_fields = ('price','name')
    # date_hierarchy = 'date_close'
admin.site.register(StatusEducation,StatusEducationAdmin)


class StandartProfessionAdmin(admin.ModelAdmin):
    #list_filter = ('business',)
    list_display = ('name','standard', 'description')
    #search_fields = ('business','name')
    #date_hierarchy = 'date_end'

admin.site.register(StandardProfession,StandartProfessionAdmin)

class GroupEducationAdmin (admin.ModelAdmin):
    list_filter = ('date_start',)
    list_display = ('name', 'education','standard_profession', 'date_start', 'date_end')
    search_fields = ('name','education',)
    date_hierarchy = 'date_end'

admin.site.register(GroupEducation,GroupEducationAdmin)

class GroupEducationSpecialistAdmin (admin.ModelAdmin):
    list_filter = ('specialist','date_start')
    list_display = ('group_education','specialist','date_start', 'date_end')
    search_fields = ('specialist','group_education')
    date_hierarchy = 'date_end'

admin.site.register(GroupEducationSpecialist,GroupEducationSpecialistAdmin)


class GroupEducationCompetenceAdmin (admin.ModelAdmin):
    list_filter = ('competence',)
    list_display = ('group_education','competence',)
    #search_fields = ('vacancy__name','education__name')
    #date_hierarchy = 'date_end'

admin.site.register(GroupEducationCompetence,GroupEducationCompetenceAdmin)


