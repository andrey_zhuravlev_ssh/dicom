(function($) {
    "use strict"
    jQuery(document).ready(function() {
        // // Navigation for Mobile Device
        $('.custom-navbar').on('click', function(){
            $('.main-menu ul').slideToggle(500);
        });
        $(window).on('resize', function(){
            if ( $(window).width() > 767 ) {
                $('.main-menu ul').removeAttr('style');
            }
        });

        // Employee Slider
        $('.employee-slider').owlCarousel({
            loop: true,
            margin: 20,
            autoplay: true,
            autoplayTimeout: 2000,
            autoplayHoverPause: true,
            nav: false,
            dots: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 1
                },
                768: {
                    items: 1
                },
                992: {
                    items: 2
                }
            }
        });

        // Nice Select
        $('select').niceSelect();

        // Range Slider
        $("#range").ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 0,
            max: 5000,
            from: 1000,
            to: 4000,
            type: 'double',
            step: 1,
            prefix: "$",
            grid: true
        });

        // Google Map
        if ( $('#mapBox').length ){
            var $lat = $('#mapBox').data('lat');
            var $lon = $('#mapBox').data('lon');
            var $zoom = $('#mapBox').data('zoom');
            var $marker = $('#mapBox').data('marker');
            var $info = $('#mapBox').data('info');
            var $markerLat = $('#mapBox').data('mlat');
            var $markerLon = $('#mapBox').data('mlon');
            var map = new GMaps({
            el: '#mapBox',
            lat: $lat,
            lng: $lon,
            scrollwheel: false,
            scaleControl: true,
            streetViewControl: false,
            panControl: true,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            zoom: $zoom,
                styles: [
                    {
                        "featureType": "water",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#dcdfe6"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "stylers": [
                            {
                                "color": "#808080"
                            },
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#dcdfe6"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "weight": 1.8
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#d7d7d7"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ebebeb"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#a7a7a7"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#efefef"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#696969"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#737373"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#d6d6d6"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {},
                    {
                        "featureType": "poi",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    }
                ]
            });
        }
		
		// Редактирую шаблон
		
		// табы на главной 
		$(".tabs p").click(function(){
			if(!$(this).hasClass("active")){
				$(".tabs p").each(function(){
					$(".tabs p").removeClass("active");
				});
				$(this).addClass("active");
				var active = $(this).attr("data-id");
				$(".fills .fill").each(function(){
					$(this).removeClass("active");
					if($(this).attr("data-id") == active){
						$(this).addClass("active");
					}
				});
			}
		});
		
		// появление форм
		
		$("header .link").click(function(){
			$(".ordercallform").show();
			$(".backgroundhide").show();
		});



	//Моя форма
	
	   var widthM = window.screen.width;
    if (widthM < 600) {
        $("head").append("<meta name=\"viewport\" content=\"width=500, user-scalable=yes\">");
    } else if (widthM < 800) {
        $("head").append("<meta name=\"viewport\" content=\"width=600, user-scalable=yes\">");
    }
    $(".ordercallform .backgroundhide").hide();
    $(".ordercallform .errorbot").hide();

    /* Скрыть/показать форму*/
    $(".ordercallink").click(function () {
        $(".ordercallform .backgroundhide").fadeIn(50);
    });
    $(".ordercallform .box_shadow").click(function () {
        $(".ordercallform .backgroundhide").fadeOut(50);
        $(".ordercallform .message").hide();
		
    });
    $(".ordercallform .close").click(function () {
        $(".ordercallform .backgroundhide").fadeOut(50);
		$(".ordercallform .message").hide();
    });

    /* Проверить обязательные поля */
    $(".ordercallform .message").each(function () {
        $(this).prev().addClass("important empty");
    });

    /* Проверить согласие с политикой */
    if ($("div").is(".ordercallform .agreeblock")) {
        $(".ordercallform .agreeform").on("change", function () {
            if ($(".ordercallform .agreeform").prop("checked")) {
                $(".ordercallform input[type=submit]").attr("disabled", false);

            } else {
                $(".ordercallform input[type=submit]").attr("disabled", true);
            }
        });
    } else {
        $(".ordercallform input[type=submit]").attr("disabled", false);
    }

    /* Маска для телефона, проверка номера (работает, надо раскомментировать)*/
    //$(".ordercallform .phonemask").mask("+7 (999) 999-9999");

    //$(".ordercallform .phonemask").on("focus click", function() {
    //  $(this)[0].setSelectionRange(4, 4);
    //})

    /* Проверка номера без маски */
    var re = /^\d[\d\(\)\ -]{4,14}\d$/;

    function validPhone() {
        var re = new RegExp(/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/);
        var phone = $(".ordercallform .namefield input[name=phone]").val();
        var valid = re.test(phone);
        if (valid) {
            return true;
        } else {
            return false;
        }
    }

    /* Скрыть/показать сообщения, что поле не заполнено */
    function checkInput() {
        $(".ordercallform input[type=text].important").each(function () {
            if ($(this).val() != "") {
                $(this).removeClass("empty");
                $(this).next().hide();
            } else {
                $(this).addClass("empty");
            }
        });
    }

    /* Обработка клика на кнопку отправки формы */
    $(".ordercallform input[type=submit]").click(function () {
        if ($(".ordercallform input[type=submit]").attr("disabled", true)) {
            $(".ordercallform input[type=submit]").removeAttr("disabled");
        }
        checkInput();
        validPhone();
        var empty = $(".ordercallform .empty").length;

        if (empty > 0 || validPhone() == false) {
            $(".ordercallform .empty").each(function () {
                /* Отмена отправки формы */
                $(".ordercallsent").submit(function (event) {
                    event.preventDefault();
                });
                $(this).next().show();
                $(this).next().text("Это поле обязательное");
            });
            if (validPhone() == false && $(".ordercallform .namefield input[name=phone]").val() != "") {
                $(".ordercallform .namefield input[name=phone]").next().show();
                $(".ordercallform .namefield input[name=phone]").next().text("Введен некорректный номер");
            }
        } else {
            $(".ordercallsent").submit(function () {
                var str = $(this).serialize();
                /* Отправка сообщения на ajax */
                $.ajax({
                    type: "POST",
                    url: "/ordercall/send/", // Поменять потом!!!
                    data: str,
                    dataType: "json",
                    success: function (msg) {
                        result = $.parseJSON(msg);
                        $(".ordercallform .hideform").hide();
                        $(".ordercallform .form").css({"margin-top": "100px"});
                        $(".ordercallform .headerinfo").html("Форма отправлена успешно");
                    },
                    error: function (msg) { // Данные не отправлены
                        //$(".ordercallform .headerinfo").html("Форма не отправлена ");
                        $(".ordercallform .errorbot").show();
                    }
                });
                return false;
            });
        }
    });

    /* Ползунок со временем */
    $(".ordercallform .polzunok").slider({
        animate: "slow",
        range: true,
        min: 12,
        step: 1,
        max: 21,
        values: [14, 19],
        slide: function (event, ui) {
            var one = ui.values[0];
            var two = ui.values[1];
            $(".ordercallform input[name=timefrom]").val(ui.values[0]);
            $(".ordercallform input[name=timeto]").val(ui.values[1]);
        }
    });

    $(".ordercallform input[name=timefrom]").val($(".ordercallform .polzunok").slider("values", 0));
    $(".ordercallform input[name=timeto]").val($(".ordercallform .polzunok").slider("values", 1));
    var timeprint = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21];

    if ($("div").is("#resulttime")) {
        for (i = 0; i < timeprint.length; i++) {
            document.getElementById("resulttime").innerHTML += "<div>" + timeprint[i] + "</div>";
        }
    }

    /* Скрыть капчу (невидимую)*/
    if ($("div").is(".ordercallform .wa-captcha .g-recaptcha")) {
        if ($(".ordercallform .wa-captcha .g-recaptcha").attr("data-size") === "invisible") {
            $(".ordercallform .wa-captcha .g-recaptcha").css({"height": "0px"});
        }
    }

 
		

    });

    jQuery(window).on('load', function() {
        // WOW JS
        new WOW().init();
        // Preloader
		$('.preloader').fadeOut(500);
    });
})(jQuery);
