# -*- coding: utf-8 -*-
from django.db import models


class Competence(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")

    # name = models.CharField(verbose_name="Порядковый номер", blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Компетенции"


# Reference table for city
class City(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Населенный пункт"