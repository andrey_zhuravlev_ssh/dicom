from django.contrib import admin

from competence.models import Competence,City


admin.site.register(Competence)


class CompetenceAdmin(admin.ModelAdmin):
    #list_filter = ('city',)
    list_display = ('name','description',)
    search_fields = ('name',)
    # date_hierarchy = 'date_close'

class CityAdmin(admin.ModelAdmin):
    #list_filter = ('business__name', 'education__name')
    list_display = ('name',)
    search_fields = ('name',)
    # date_hierarchy = 'date_close'

admin.site.register(City,CityAdmin)