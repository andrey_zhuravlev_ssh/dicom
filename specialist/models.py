# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from competence.models import Competence, City


# Reference table for status of university (gold, silver, simple, block etc.)
class StatusSpecialist(models.Model):
    name = models.CharField(max_length=1024, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")
    price = models.FloatField(verbose_name="Цена", blank=True, null=True)
    count_test = models.IntegerField(verbose_name="Минимальное количество комптенций (для бесплатной подписки)",
                                     blank=True, null=True)
    rule_use_free = models.TextField(verbose_name="Особые требования (для бесплатной подписки)",
                                     blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = u"Статус специалиста (в системе)"


class Specialist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(verbose_name="Описание")
    image = models.ImageField(upload_to='uploads/images', verbose_name="Картинка", blank=True, null=True)
    resume = models.URLField(verbose_name="Ссылка на резюме", blank=True, null=True)
    city = models.ForeignKey(City, verbose_name="Населенный пункт", blank=True, null=True)
    address = models.CharField(max_length=2048, verbose_name="Адрес", blank=True, null=True)
    status = models.ForeignKey(StatusSpecialist, verbose_name="Статус (подписка)", blank=True, null=True)
    geo = models.CharField(max_length=1024, verbose_name="Координаты", blank=True, null=True)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        ordering = ["id"]
        verbose_name_plural = u"Специалист"



