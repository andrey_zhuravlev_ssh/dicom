from django.contrib import admin

from specialist.models import Specialist,StatusSpecialist


class SpecialistAdmin(admin.ModelAdmin):
    list_filter = ('city',)
    list_display = ('user', 'resume', 'city', 'address','status')
    # search_fields = ('user_get_full_name', 'city__name',)


admin.site.register(Specialist,SpecialistAdmin)

class StatusSpecialistAdmin(admin.ModelAdmin):
    list_filter = ('price',)
    list_display = ('name','description', 'price','count_test','rule_use_free')
    search_fields = ('name','price')

admin.site.register(StatusSpecialist,StatusSpecialistAdmin)







