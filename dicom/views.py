# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render
from django.shortcuts import redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect


def logout_view(request):
    logout(request)
    return redirect('/')


# @login_required
def index(request, template_name="index.html"):
    return render(request, template_name, locals())


def global_views(request):
    return locals()


@login_required
def lk(request):
    try:
        request.user.business
        return HttpResponseRedirect("/lk_business")
    except:
        pass
    try:
        request.user.education
        return HttpResponseRedirect("/lk_education")
    except:
        pass
    return HttpResponseRedirect("/lk_specialist")