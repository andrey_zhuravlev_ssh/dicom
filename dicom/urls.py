from django.conf.urls import include, url
from django.contrib import admin

from dicom.views import index, logout_view, lk
from specialist.views import lk_specialist
from education.views import lk_education
from business.views import lk_business, business_order, business_vacancy

import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'auth.views.login'),
    url(r'^accounts/logout/$', logout_view),
    url(r'^$', index),
    url(r'^lk/$', lk),
    url(r'^lk_specialist/$', lk_specialist),
    url(r'^lk_education/$', lk_education),
    url(r'^lk_business/$', lk_business),

    url(r'^business_order/$', business_order),
    url(r'^business_vacancy/$', business_vacancy),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    # url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    # url(r'^(?P<path>.[^/]*)$', 'django.views.static.serve', {'document_root': settings.WWW_ROOT}),
]
